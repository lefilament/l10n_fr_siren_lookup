A button "Pre-Fill / Update" is added on company form views.

By default, the search field is filled with Company name. To get more accurate results, you may want to add the City name where the company is registered then click on "Search"

A list of company is proposed. You may want to click on one in order to see corresponding information or directly selecting company from treeview. Once a company is selected, wizard goes away and corresponding company information are added/updated in "Legal Infos" page from NoteBook.

