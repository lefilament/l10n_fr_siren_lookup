This modules looks for address and contact information for a company into `SIRENE database <https://data.opendatasoft.com>`

A button is added in Contact form, which creates a wizard that would update the following information:

* Company Name
* Address
* Postal Code
* City
* Legal Type
* SIREN & SIRET
* APE Code and label
* Creation date
* # Staff
* Company Category

This module relies on `INSEE's API <https://data.opendatasoft.com/api/records/1.0/search/>`
